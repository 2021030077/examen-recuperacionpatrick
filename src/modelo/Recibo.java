package modelo;
public class Recibo {
    private int numRecibo;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private float costoKilowatts;
    private float kiloWattscon; 
    private String fecha;
    public Recibo() {

    }

    public Recibo(int numRecibo, String nombre, int tipoServicio, float costoKilowatts, float kiloWattscon,String fecha,String domicilio) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.tipoServicio = tipoServicio;
        this.costoKilowatts = costoKilowatts;
        this.kiloWattscon = kiloWattscon;
        this.fecha= fecha;
        this.domicilio= domicilio;
    }

    public Recibo(Recibo R){
        numRecibo=R.numRecibo;
        nombre=R.nombre;
        tipoServicio=R.tipoServicio;
        costoKilowatts=R.costoKilowatts;
        kiloWattscon=R.kiloWattscon;
        
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public float getCostoKilowatts() {
        return costoKilowatts;
    }

    public void setCostoKilowatts(float costoKilowatts) {
        this.costoKilowatts = costoKilowatts;
    }

    public float getKiloWattscon() {
        return kiloWattscon;
    }

    public void setKiloWattscon(float kiloWattscon) {
        this.kiloWattscon = kiloWattscon;
    }
    
    public float SubTotal(){
        return this.kiloWattscon*this.costoKilowatts;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    public float Impuesto() {
        return SubTotal() * 0.16f;
    }
    public float TotalPagar(){
        return SubTotal()+Impuesto();
    }
}
