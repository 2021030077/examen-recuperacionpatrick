package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Recibo;
import vista.dlgRecibo;
public class Controlador implements ActionListener{
    private Recibo R;
    private dlgRecibo vista;
    public Controlador(Recibo R,dlgRecibo vista){
        this.R =R;
        this.vista =vista;
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    private void iniciarVista(){
        vista.setTitle(":: Productos ::");
        vista.setSize(480,620);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==vista.btnNuevo){
            
            vista.txtNombre.setEnabled(true);
            vista.txtNumRecibo.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtKilowatssConsumido.setEnabled(true);
            vista.cboServicio.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
        }
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        if(e.getSource()==vista.btnGuardar){
            try{
                if(Integer.parseInt(vista.txtNumRecibo.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de recibo valido");
                }
                
                if(Float.parseFloat(vista.txtKilowatssConsumido.getText()) <= 0){
                    throw new IllegalArgumentException("Ingrese un numero de Kilowatts valido");
                }
                R.setNumRecibo(Integer.parseInt(vista.txtNumRecibo.getText()));
                R.setNombre(vista.txtNombre.getText());
                R.setFecha(vista.txtFecha.getText());
                R.setDomicilio(vista.txtDomicilio.getText());
                R.setCostoKilowatts(Float.parseFloat(vista.txtCostoKilowatts.getText()));
                R.setKiloWattscon(Float.parseFloat(vista.txtKilowatssConsumido.getText()));
                R.setTipoServicio(vista.cboServicio.getSelectedIndex());
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex2.getMessage());
                return;
            }
            

            vista.btnCancelar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
        }
        if(e.getSource()==vista.btnMostrar){
            vista.txtNombre.setText(R.getNombre());
            vista.txtFecha.setText(R.getFecha());
            vista.txtDomicilio.setText(R.getDomicilio());
            vista.txtNumRecibo.setText(Integer.toString(R.getNumRecibo()));
            vista.txtSubTotal.setText(Float.toString(R.SubTotal()));
            vista.txtImpuesto.setText(Float.toString(R.Impuesto()));
            vista.txtTotalPagar.setText(Float.toString(R.TotalPagar()));
            
        }
        
        if(e.getSource()==vista.btnCancelar){
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumRecibo.setEnabled(false);
            vista.txtSubTotal.setEnabled(false);
            vista.txtTotalPagar.setEnabled(false);
            vista.txtImpuesto.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
        }
        
        if(e.getSource()==vista.btnLimpiar){
            vista.txtDomicilio.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtNumRecibo.setEnabled(false);
            vista.txtSubTotal.setEnabled(false);
            vista.txtTotalPagar.setEnabled(false);
            vista.txtImpuesto.setEnabled(false);
        }
        }
        public static void main(String[] args) {
            Recibo D = new Recibo();
            dlgRecibo vista = new dlgRecibo(new JFrame(),true);
            Controlador contra = new Controlador (D,vista);
            contra.iniciarVista();
    }

}
